import React, { Component } from 'react';
import AppHeader from '../AppHeader';
import SearchPanel from '../SearchPanel';
import TodoList from '../TodoList';
import ItemAddForm from '../ItemAddForm';
import ItemStatusFilter from '../ItemStatusFilter';
import {ONLY_DONE, ONLY_ACTIVE} from '../../constants';
import './App.css'

export default class App extends Component {
  startId = 1;

  state = {
    filter: null,
    search: '',
    todoData: [
      this.createTodoItem('Drink Coffee'),
      this.createTodoItem('Build React App'),
      this.createTodoItem('Have a lunch'),
    ]
  };

  createTodoItem(label) {
    return {
      label,
      important: false,
      id: this.startId++,
      done: false,
    }
  }

  deleteItem = (id) => {
    this.setState(({todoData})=>{
      const ind = todoData.findIndex((el) => el.id === id);
      const newArray = [...todoData.slice(0, ind), ...todoData.slice(ind + 1)]
      return {
        todoData: newArray
      }
    });
  }

  addItem = (text) => {
    const newItem = this.createTodoItem(text)

    this.setState(({todoData}) => {
      const newArray = [...todoData, newItem];
      return {
        todoData: newArray
      }
    })
    
  }

  toogleProperty(arr, id, propName) {
      const ind = arr.findIndex((el) => el.id === id);
      const oldItem = arr[ind];
      const newItem = {...oldItem, [propName]: !oldItem[propName]};
      return [...arr.slice(0, ind), newItem, ...arr.slice(ind + 1)]; 
  }

  onToogleDone = (id) => {
    this.setState(({todoData})=>{
      return{
        todoData: this.toogleProperty(todoData, id, "done")
      }
    })
  }

  onToogleImportant = (id) => {
    this.setState(({todoData})=>{
      return{
        todoData: this.toogleProperty(todoData, id, "important")
      }
    })
  }

  onFiltered = filterValue => {
    this.setState({filter: filterValue})
  }

  onChangeSearch = value => {
    this.setState({search: value})
  }

  get todos() {
    const {todoData, filter, search} = this.state;
    let items = todoData;
    if (filter) {
      if (filter === ONLY_ACTIVE) {
        items = items.filter(item => !item.done)
      } else if (filter === ONLY_DONE) {
        items = items.filter(item => item.done)
      }
    }
    if (search) {
      items = items.filter(item => item.label.toLowerCase().includes(search.toLowerCase()))
    }
    return items
  }

  render(){
    const {todoData, search, filter} = this.state;
    const todos = this.todos;
    const doneCount = todoData.filter((el) => el.done).length;
    const todoCount = todoData.length - doneCount;

    return (
      <div className="todo-app">
        <AppHeader toDo={todoCount} done={doneCount}/>
        <div className="top-panel d-flex">
            <SearchPanel onChangeSearch={this.onChangeSearch} value={search}/>
            <ItemStatusFilter filter={filter} onFiltered={this.onFiltered}/> 
          </div>
        <TodoList 
          todos={todos} 
          onDeleted={this.deleteItem}
          onToogleDone={this.onToogleDone}
          onToogleImportant={this.onToogleImportant}
        />
        <ItemAddForm onItemAdded={this.addItem}/>
      </div>
    );
  }
};