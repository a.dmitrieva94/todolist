import React from 'react';
import TodoListItem from '../TodoListItem/TodoListItem';
import './TodoList.css'

const TodoList = ({todos, onDeleted, onToogleDone, onToogleImportant}) => {
    return (
        <ul className='list-group todo-list'>
            {todos.map((item) => { 
                const {id, ...itemProps} = item
                return (
                    <li key={id} className='list-group-item'> 
                        <TodoListItem {...itemProps} 
                            onDeleted={()=>{onDeleted(id)}} 
                            onToogleDone={()=>{onToogleDone(id)}} 
                            onToogleImportant={()=>{onToogleImportant(id)}}
                        /> 
                    </li>
                )
            })}
        </ul>
    );
};

export default TodoList;