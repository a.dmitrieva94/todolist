import React, { Component } from 'react';
import {ONLY_DONE, ONLY_ACTIVE} from '../../constants';
import './ItemStatusFilter.css'

export default class ItemStatusFilter extends Component {
    items = [
        {
            label: 'All',
            filter: null
        },
        {
            label: 'Active',
            filter: ONLY_ACTIVE
        },
        {
            label: 'Done',
            filter: ONLY_DONE
        },
    ]
    render(){
        const {onFiltered, filter} = this.props

        const buttons = this.items.map(el => {
            return (
                <button type='button' 
                    key={el.label}
                    onClick={() => onFiltered(el.filter)} 
                    className={`btn ${filter === el.filter ? 'btn-primary' : 'btn-outline-info'}`}>
                    {el.label}
                </button>
            )
        });

        return (
            <div className='btn-group'>
                {buttons}
            </div>
        );
    }
};