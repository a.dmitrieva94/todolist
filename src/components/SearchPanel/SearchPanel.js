import React from 'react';
import './SearchPanel.css'

const SearchPanel = ({onChangeSearch, value}) => {

    return <input type="text" onChange={e => onChangeSearch(e.target.value)} value={value} className="form-control search-input" placeholder="type to search" />;
};

export default SearchPanel;